<?php

namespace Drupal\simple_redirect;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Provides routes for Simple Redirect entities.
 *
 * @see Drupal\Core\Entity\Routing\AdminHtmlRouteProvider
 * @see Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider
 */
class SimpleRedirectHtmlRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);

    $entity_type_id = $entity_type->id();

    if ($collection_route = $this->getCollectionRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.collection", $collection_route);
    }

    if ($add_form_route = $this->getAddFormRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.add-form", $add_form_route);
    }

    return $collection;
  }

  /**
   * Gets the collection route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getCollectionRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasListBuilderClass()) {
      if ($entity_type->hasLinkTemplate('collection')) {
        return $this->_helperProvideHTMLRoute($entity_type, 'collection');
      }
      if ($entity_type->hasLinkTemplate('add-form')) {
        return $this->_helperProvideHTMLRoute($entity_type, 'add-form');
      }
    }
    return NULL;
  }

  /**
   * Helper to provide HTML Route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @param String $form_id
   *   Form ID
   *
   * @return \Symfony\Component\Routing\Route
   *   The generated route, if available.
   */
  private function _helperProvideHTMLRoute(EntityTypeInterface $entity_type, $form_id) {
    $route = new Route($entity_type->getLinkTemplate($form_id));
    $route
      ->setDefaults([
        '_entity_list' => $entity_type->id(),
        // Make sure this is not a TranslatableMarkup object as the
        // TitleResolver translates this string again.
        '_title' => (string) $entity_type->getLabel(),
      ])
      ->setRequirement('_permission', $entity_type->getAdminPermission())
      ->setOption('_admin_route', TRUE);
    return $route;
   }

}
