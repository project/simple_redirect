<?php

namespace Drupal\simple_redirect\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Simple Redirect entities.
 */
interface SimpleRedirectInterface extends ConfigEntityInterface {

  /**
   * Get the from URL.
   *
   * @return string
   *   String with the "from" value.
   */
  public function getFrom();

  /**
   * Get the to URL.
   *
   * @return string
   *   Return the string with "to" value.
   */
  public function getTo();

}
