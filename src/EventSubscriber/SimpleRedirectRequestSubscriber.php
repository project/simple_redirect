<?php

namespace Drupal\simple_redirect\EventSubscriber;

use Drupal\Core\Url;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Default Request class for Event Event Subscriber.
 *
 * @package Drupal\simple_redirect
 */
class SimpleRedirectRequestSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST] = ['onKernelRequestSimpleRedirect'];

    return $events;
  }

  /**
   * This method is called whenever the kernel.request event is dispatched.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The event request from event dispacher.
   */
  public function onKernelRequestSimpleRedirect(RequestEvent $event) {
    $request = clone $event->getRequest();

    $redirect_url = $this->getRedirectUrl($request->getRequestUri());
    if ($redirect_url && $redirect_url != NULL) {
      $event->setResponse(new RedirectResponse($redirect_url, 301));
    }
  }

  /**
   * Helper to check from url in the config, if exist return redirect url.
   *
   * @param string $fromUrl
   *   String with from URL value to be compared.
   *
   * @return \Drupal\Core\GeneratedUrl|null|string
   *   Return URL (cast to string), or return NULL on failure.
   */
  private function getRedirectUrl($fromUrl) {
    $simple_redirect_conf = \Drupal::entityTypeManager()->getStorage('simple_redirect')->loadMultiple();
    foreach ($simple_redirect_conf as $conf) {
      if ($conf->getFrom() == $fromUrl) {
        return Url::fromUserInput($conf->getTo())->toString();
      }
    }
    return NULL;
  }

}
